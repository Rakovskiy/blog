<?php

return [
    'components' => [
        'view' => [
            'class'        => 'app\system\components\View',
            'templatePath' => dirname(dirname(__DIR__)) . '/app/views'
        ]
    ]
];