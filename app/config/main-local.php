<?php

return [
    'components' => [
        'db' => [
            'class'    => 'app\system\components\Db',
            'dsn'      => 'mysql:host=localhost;dbname=db_test_blog;charset=utf8mb4',
            'user'     => 'root',
            'pass'     => '1122334455',
        ],
    ]
];