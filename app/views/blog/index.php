<?php

/* @var $this \app\system\components\View */
/* @var $count int */
/* @var $all array */
?>

    <a href="/blog/create.php" class="text-uppercase text-center btn-create btn-block bg-info">
        Добавить запись
    </a>

<?php if ($count == 0): ?>
    <article class="single-blog">
        <div class="post-content">
            <div class="entry-header text-center text-uppercase">
                <h2>Записи не найдены!</h2>
            </div>
        </div>
    </article>
<?php else: ?>
    <?php foreach ($all as $model): ?>
        <?php
        $content = mb_strlen($model['content']) > 250 ?
            (mb_substr($model['content'], 0, 250) . '...') : $model['content']
        ?>
        <article class="single-blog">
            <div class="post-thumb">
                <a href="/blog/view.php?id=<?= $model['id'] ?>">
                    <?php if (empty($model['preview']) === false): ?>
                        <img src="/files/blog/<?= $model['preview'] ?>" alt="">
                    <?php else: ?>
                        <img src="/assets/images/post-thumb-1.jpg" alt="">
                    <?php endif; ?>
                </a>
            </div>
            <div class="post-content">
                <div class="entry-header text-center text-uppercase">
                    <h2>
                        <a href="/blog/view.php?id=<?= $model['id'] ?>">
                            <?= htmlspecialchars($model['title']) ?>
                        </a>
                    </h2>
                </div>

                <div class="entry-content">
                    <p>
                        <?= nl2br(htmlspecialchars($content)) ?>
                    </p>
                </div>

                <div class="continue-reading text-center text-uppercase">
                    <a href="/blog/view.php?id=<?= $model['id'] ?>">
                        Продолжить чтение!
                    </a>
                </div>

                <div class="post-meta">
                    <ul class="pull-left list-inline author-meta">
                        <li class="date">
                            Добавлено <?= date('d.m в H:i', $model['created_at']) ?>
                        </li>
                    </ul>
                    <ul class="pull-right list-inline social-share">
                        <li><a href=""><i class="fa fa-facebook"></i></a></li>
                        <li><a href=""><i class="fa fa-twitter"></i></a></li>
                        <li><a href=""><i class="fa fa-pinterest"></i></a></li>
                        <li><a href=""><i class="fa fa-google-plus"></i></a></li>
                        <li><a href=""><i class="fa fa-instagram"></i></a></li>
                    </ul>
                </div>
            </div>
        </article>
    <?php endforeach; ?>
<?php endif; ?>