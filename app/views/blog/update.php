<?php
/* @var $this \app\system\components\View */
/* @var $model array */
/* @var $saveData array */
?>

<div class="leave-comment">
    <h4>
        <a href="/blog/view.php?id=<?= $model['id'] ?>">
            <i class="fa fa-long-arrow-left"></i>
        </a>
        Изменить запись
    </h4>
    <?= App::$get->view->renderPartial('blog/_form', [
        'model'    => $model,
        'saveData' => $saveData,
    ]) ?>
</div>