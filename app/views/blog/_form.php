<?php
/* @var $this \app\system\components\View */
/* @var $model array */
/* @var $saveData array */

$title   = $_POST['title'] ?? $model['title'] ?? null;
$content = $_POST['content'] ?? $model['content'] ?? null;
?>

<form enctype="multipart/form-data" class="form-horizontal contact-form" method="post" action="#">

    <?php if (isset($saveData['error']) && $saveData['error'] !== null): ?>
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-ban"></i> Вы допустили ошибку!</h4>
            <?= htmlspecialchars($saveData['error']) ?>
        </div>
    <?php endif; ?>

    <div class="form-group">
        <div class="col-md-12">
            <label>Превью</label>
            <input type="file" name="preview" />
        </div>
    </div>

    <div class="form-group">
        <div class="col-md-12">
            <label>Заголовок</label>
            <input type="text" class="form-control" id="title" name="title" value="<?= htmlspecialchars($title) ?>" required>
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-12">
            <label>Содержимое</label>
            <textarea class="form-control" rows="6" name="content" required><?= htmlspecialchars($content) ?></textarea>
        </div>
    </div>
    <input type="submit" value="Сохранить" name="submit" class="btn send-btn">
</form>