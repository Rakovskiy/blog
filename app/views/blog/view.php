<?php

/* @var $this \app\system\components\View */
/* @var $model array */
?>

<article class="single-blog">
    <div class="post-thumb">
        <?php if (empty($model['preview']) === false): ?>
            <img src="/files/blog/<?= $model['preview'] ?>" alt="">
        <?php else: ?>
            <img src="/assets/images/post-thumb-1.jpg" alt="">
        <?php endif; ?>
    </div>
    <div class="post-content">
        <div class="entry-header text-center text-uppercase">
            <h2>
                <?= htmlspecialchars($model['title']) ?>
            </h2>
        </div>

        <div class="entry-content">
            <p>
                <?= nl2br(htmlspecialchars($model['content'])) ?>
            </p>
        </div>

        <div class="entry-content text-center">
            <a href="/blog/update.php?id=<?= $model['id'] ?>" class="text-uppercase text-center btn btn-success btn-flat">
                Изменить запись
            </a>
            <a onclick="return confirm('Вы уверены, что хотите удалить запись?')" href="/blog/delete.php?id=<?= $model['id'] ?>" class="text-uppercase text-center btn btn-primary btn-flat">
                Удалить запись
            </a>
        </div>

        <div class="post-meta">
            <ul class="pull-left list-inline author-meta">
                <?php if ($model['updated_at'] > $model['created_at']): ?>
                    <li class="date">
                        Изменено <?= date('d.m в H:i', $model['updated_at']) ?>
                    </li>
                <?php else: ?>
                    <li class="date">
                        Добавлено <?= date('d.m в H:i', $model['created_at']) ?>
                    </li>
                <?php endif; ?>
            </ul>
            <ul class="pull-right list-inline social-share">
                <li><a href=""><i class="fa fa-facebook"></i></a></li>
                <li><a href=""><i class="fa fa-twitter"></i></a></li>
                <li><a href=""><i class="fa fa-pinterest"></i></a></li>
                <li><a href=""><i class="fa fa-google-plus"></i></a></li>
                <li><a href=""><i class="fa fa-instagram"></i></a></li>
            </ul>
        </div>
    </div>
</article>