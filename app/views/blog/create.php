<?php
/* @var $this \app\system\components\View */
/* @var $model array */
/* @var $saveData array */
?>

<div class="leave-comment">
    <h4>
        <a href="/">
            <i class="fa fa-long-arrow-left"></i>
        </a>
        Добавить запись
    </h4>
    <?= App::$get->view->renderPartial('blog/_form', [
        'model'    => $model,
        'saveData' => $saveData
    ]) ?>
</div>