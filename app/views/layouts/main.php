<?php
/* @var $this \app\system\components\View */
/* @var $content string */
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <!-- Document Settings -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <!-- Page Title -->
    <title>Блог</title>
    <!-- Fonts -->
    <link
            href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700,700i%7cOswald:300,400,500,600,700%7cPlayfair+Display:400,400i,700,700i"
            rel="stylesheet">
    <!-- Styles -->
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="/assets/css/slick-theme.css">
    <link rel="stylesheet" href="/assets/css/slick.css">
    <link rel="stylesheet" href="/assets/main.css">
    <!-- HTML5 shim and Respond.js IE9 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="/assets/js/html5shiv.js"></script>
    <script src="/assets/js/respond.js"></script>
    <![endif]-->
</head>
<body>

<header class="kotha-menu marketing-menu">
    <nav class="navbar  navbar-default">
        <div class="container">
            <div class="menu-content">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#myNavbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="top-social-icons list-inline pull-right">
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                        <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                    </ul>
                    <ul class="nav navbar-nav text-uppercase pull-left">
                        <li><a href="/">Мой блог</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </nav>
</header>

<div class="kotha-default-content">
    <div class="container">
        <div class="row">
            <div class="col-sm-8">
                <?= $content ?>
            </div>
            <div class="col-sm-4">
                <div class="kotha-sidebar">
                    <aside class="widget about-me-widget  text-center">
                        <div class="about-me-content">
                            <div class="about-me-img">
                                <img src="/assets/images/rakovskiy.jpg" alt="" class="img-me img-circle">
                                <h2 class="text-uppercase">Раковский Андрей</h2>
                                <p>
                                    Занимаюсь WEB разработкой.
                                    <br>
                                    Пишу на Yii2 Framework и PHP7.
                                </p>
                            </div>
                        </div>
                        <div class="social-share">
                            <ul class="list-inline">
                                <li><a class="s-facebook" href=""><i class="fa fa-facebook"></i></a></li>
                                <li><a class="s-twitter" href=""><i class="fa fa-twitter"></i></a></li>
                                <li><a class="s-google-plus" href=""><i class="fa fa-google-plus"></i></a></li>
                                <li><a class="s-linkedin" href=""><i class="fa fa-linkedin"></i></a></li>
                                <li><a class="s-instagram" href=""><i class="fa fa-instagram"></i></a></li>
                            </ul>
                        </div>
                    </aside>
                    <aside class="widget news-letter-widget">
                        <h2 class="widget-title text-uppercase text-center">Подписаться на обновления</h2>
                        <form action="#">
                            <input type="email" placeholder="Ваш email" required>
                            <a
                                    href="#subscribe"
                                    class="text-uppercase text-center btn btn-subscribe"
                                    onclick="alert('Данная функция не предусмотрена в ТЗ!')"
                            >
                                Подписаться
                            </a>
                        </form>
                    </aside>
                    <aside class="widget add-widget">
                        <a target="_blank" href="http://mltop.net/1puu2m7">
                            <h2 class="widget-title text-uppercase text-center">Хочешь увеличить член?</h2>
                            <p class="text-center">Просто нужно добавить в...</p>
                            <div class="add-image">
                                <img src="/assets/images/soda.jpg" alt="">
                            </div>
                        </a>
                    </aside>
                </div>
            </div>
        </div>
    </div>
</div>

<footer>
    <div class="container-fluid text-center ft-copyright">
        <p>
            &copy; <?= date('Y') ?> - Мой блог
        </p>
    </div>
</footer>

<div class="scroll-up">
    <a href="#"><i class="fa fa-angle-up"></i></a>
</div>

<!--//Script//-->
<script src="/assets/js/jquery-1.12.4.min.js"></script>
<script src="/assets/js/bootstrap.min.js"></script>
<script src="/assets/js/slick.min.js"></script>
<script src="/assets/js/main.js"></script>

</body>
</html>