<?php

namespace app\models;

use app;

/**
 * Class Blog
 * @package app\models
 */
class Blog {
    /**
     * Получаем одну запись блога по ID
     * @param $id
     * @return array|false
     */
    public static function findOne($id)
    {
        $query = App::$get->db->query("SELECT * FROM `blog` WHERE `id` = :id", [
            ':id' => $id
        ]);

        return $query->fetch();
    }

    /**
     * Получаем все запись блога, учитывая пагинацию
     * @return array|false
     */
    public static function findAll()
    {
        $query = App::$get->db->query("SELECT * FROM `blog` ORDER BY `id` DESC");

        return $query->fetchAll();
    }

    /**
     * Подсчитываем кол-во записей блога
     * @return string
     */
    public static function countAll()
    {
        $query = App::$get->db->query("SELECT * FROM `blog` ORDER BY `id` DESC");

        return $query->fetchColumn();
    }

    /**
     * Удаляем запись блога по ID
     * @param $id
     * @return array|false
     */
    public static function delete($id)
    {
        $model = self::findOne($id);

        if ($model === false){
            return false;
        }

        /**
         * Удаляем загруженый файл
         */
        if ($model['preview']){
            $previewPath = dirname(dirname(__DIR__)) . '/web/files/blog/' . $model['preview'];

            if (is_file($previewPath)){
                unlink($previewPath);
            }
        }

        $query = App::$get->db->query("DELETE FROM `blog` WHERE `id` = :id", [
            ':id' => $id
        ]);

        return $query->execute();
    }

    /**
     * Сохраняем запись
     * @param array $model Модель (если существует)
     * @param array $data Массив данных для сохранения (обычно это массив $_POST)
     * @return array
     */
    public static function save($model, $data)
    {
        $error   = null;
        $id      = $model['id'] ?? 0;
        $title   = $data['title'] ?? null;
        $content = $data['content'] ?? null;

        /**
         * Валидация заголовка
         */
        if (mb_strlen($title) < 5){
            $error = 'Заголовок должен быть не менее 5 символов';
        }elseif (mb_strlen($title) > 250){
            $error = 'Заголовок должен быть не более 250 символов';
        }

        /**
         * Валидация содержимого
         */
        if (mb_strlen($content) < 50){
            $error = 'Содержимое должно быть не менее 50 символов';
        }elseif (mb_strlen($content) > 5000){
            $error = 'Содержимое должно быть не более 5000 символов';
        }

        /**
         * Валидация превью
         */
        if ($id < 1 && empty($_FILES['preview']['name']) === false){
            if ($id < 1 && isset($_FILES['preview']) === false) {
                $error = 'Необходимо загрузить превью';
            }elseif (in_array($_FILES['preview']['type'], ['image/jpg', 'image/jpeg', 'image/gif', 'image/png']) === false){
                $error = 'В превью разрешено загружать только изображения';
            }elseif ($_FILES['preview']['size'] > 3000000){
                $error = 'Размер превью должен быть не более 3 мб.';
            }
        }

        /**
         * Если нет ошибок - сохраняем запись
         */
        if ($error === null){
            /**
             * Если есть превью - обрабатываем его
             */
            if (isset($_FILES['preview'])){
                $fileName   = md5(mt_rand(111111111111, 99999999999999999)) . '.jpg';
                $uploadPath = dirname(dirname(__DIR__)) . '/web/files/blog/' . $fileName;

                if (move_uploaded_file($_FILES['preview']['tmp_name'], $uploadPath)){
                    $preview = $fileName;
                }
            }

            $preview = $preview ?? $model['preview'] ?? null;

            /**
             * Если запись уже создана - изменяем её
             */
            if ($id > 0){
                App::$get->db->query(
                    "UPDATE `blog` SET `title` = :title, `content` = :content, `preview` = :preview, `updated_at` = :updated_at WHERE `id` = :id",
                    [
                        ':title'      => $title,
                        ':content'    => $content,
                        ':preview'    => $preview,
                        ':updated_at' => time(),
                        ':id'         => $id
                    ]
                );
            }else{
                /**
                 * Если запись не создана - создаём новую
                 */
                App::$get->db->query(
                    "INSERT INTO `blog` SET `title` = :title, `content` = :content, `preview` = :preview, `created_at` = :created_at, `updated_at` = :updated_at",
                    [
                        ':title'      => $title,
                        ':content'    => $content,
                        ':preview'    => $preview,
                        ':created_at' => time(),
                        ':updated_at' => time(),
                    ]
                );

                $id = App::$get->db->lastInsertId();
            }
        }

        return [
            'success' => $error === null,
            'id'      => $id,
            'error'   => $error
        ];
    }
}