<?php

namespace app\system\helpers;

/**
 * Class ReplaceArrayValue
 * Клас импортирован из Yii2
 *
 * @package app\system\helpers
 */
class ReplaceArrayValue
{
    /**
     * @var mixed
     */
    public $value;

    /**
     * ReplaceArrayValue constructor.
     * @param $value
     */
    public function __construct($value)
    {
        $this->value = $value;
    }
}