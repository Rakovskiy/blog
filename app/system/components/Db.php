<?php

namespace app\system\components;

use PDO;

/**
 * Class Db
 */
class Db extends Component {
    /**
     * @var
     */
    public $dsn;
    /**
     * @var
     */
    public $user;
    /**
     * @var
     */
    public $pass;
    /**
     * @var PDO
     */
    protected static $connection;

    /**
     * Init
     */
    public function init()
    {
        if (self::$connection instanceof PDO) {
            return;
        }

        $opt = [
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES   => false,
        ];

        self::$connection = new PDO($this->dsn, $this->user, $this->pass, $opt);
    }

    /**
     * @param $sql
     * @param array $params
     * @return \PDOStatement
     */
    public function query($sql, $params = [])
    {
        $query = self::$connection->prepare($sql);
        $query->execute($params);

        return $query;
    }

    /**
     * @param null $name
     * @return string
     */
    public function lastInsertId($name = null)
    {
        return self::$connection->lastInsertId($name);
    }
}