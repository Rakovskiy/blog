<?php

namespace app\system\components;

/**
 * Class Component
 * @package app\system\classes
 */
class Component {
    /**
     * Данный метод вызывается после инициализации объекта
     */
    public function init()
    {

    }
}