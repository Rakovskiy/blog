<?php

namespace app\system\components;

/**
 * Class View
 * @package app\system\classes
 */
class View extends Component {
    /**
     * Путь к директории с шаблонами
     * @var
     */
    public $templatePath;
    /**
     * Расширение файла шаблона
     * @var
     */
    public $templateFileExtension = 'php';

    /**
     * Подключаем представление внутри layout
     * @param $view
     * @param array $params
     * @param null $templatePath
     * @return string
     */
    public function render($view, $params = [], $templatePath = null)
    {
        return $this->renderPartial('layouts/main', [
            'this'    => $this,
            'content' => $this->renderPartial($view, $params, $templatePath)
        ]);
    }

    /**
     * Подключаем представление без layout
     * @param $view
     * @param array $params
     * @param null $templatePath
     * @return mixed
     * @throws \Exception
     */
    public function renderPartial($view, $params = [], $templatePath = null)
    {
        $path = ($templatePath ? $templatePath : $this->templatePath) . '/' . $view . '.' . $this->templateFileExtension;

        /**
         * Если файл представления не найден - выкидываем исключение
         */
        if (is_file($path) === false){
            throw new \Exception('View not found in path: "' . $path . '".');
        }

        /**
         * Импортирует переменные из массива $params, чтобы они были доступны в файле представления
         */
        $params['this'] = $this;
        extract($params);

        ob_start();
        require $path;
        $content = ob_get_clean();

        return $content;
    }
}