<?php

namespace app\system\classes;

use app\system\components\Db;
use app\system\components\View;

/**
 * Class Registry
 * @package app\system\classes
 *
 * @property Db $db
 * @property View $view
 */
class Registry {
    /**
     * @var array
     */
    protected static $data = [];

    /**
     * @param $name
     * @return mixed
     * @throws \Exception
     */
    public function __get($name)
    {
        if (isset(self::$data[$name])){
            return self::$data[$name];
        }

        throw new \Exception('The "' . $name . '" component was not found.');
    }

    /**
     * @param $key
     * @param $value
     */
    public function __set($key, $value)
    {
        self::$data[$key] = $value;
    }
}