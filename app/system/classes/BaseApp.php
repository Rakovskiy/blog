<?php

namespace app\system\classes;

use app\system\components\Component;

/**
 * Class App
 * @package app\system\classes
 */
class BaseApp {
    /**
     * @var Registry
     */
    public static $get;

    /**
     * @param $config
     * @throws \Exception
     */
    public function run($config)
    {
        self::$get = new Registry();

        /**
         * Подгружаем компоненты
         */
        if (isset($config['components'])){
            $this->setComponents($config['components']);
        }
    }

    /**
     * Загрузка компонентов
     * @param $components
     * @throws \Exception
     */
    public function setComponents($components)
    {
        foreach ($components as $name => $component){
            /**
             * Если не указан класс компонента - выкидываем исключение
             */
            if (isset($component['class']) === false){
                throw new \Exception('The "' . $name . '" component does not have the required parameter "class".');
            }

            /**
             * Создаём экземпляр класса компонента
             * @var $obj Component
             */
            $obj = new $component['class'];

            /**
             * Проверяем чтобы класс компонента должен был быть наследован от класса app\system\components\Component
             */
            if (($obj instanceof Component) === false){
                throw new \Exception('The "' . $name . '" component class must be inherited from the "app\system\components\Component" class.');
            }

            /**
             * Удаляем параметр название класса, т.к. далее мы будем приравнивать все параметры к свойствам класса (смотрите ниже)
             */
            unset($component['class']);

            /**
             * Приравниваем все параметры компонента к свойствам класса
             */
            foreach ($component as $key => $value){
                $obj->$key = $value;
            }

            $obj->init();

            /**
             * Записываем компонент в реестр приложения
             */
            self::$get->$name = $obj;
        }
    }
}