<?php

namespace app\system\exceptions;

/**
 * Class NotFoundHttpException
 * @package app\system\exceptions
 */
class NotFoundHttpException extends BaseException {
    /**
     * NotFoundHttpException constructor.
     * @param null $message
     * @param int $code
     * @param \Exception|null $previous
     */
    public function __construct($message = null, $code = 0, \Exception $previous = null)
    {
        parent::__construct(404, $message, $code, $previous);
    }
}