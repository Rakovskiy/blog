<?php

namespace app\system\exceptions;

/**
 * Class BaseException
 * @package app\system\exceptions
 */
class BaseException extends \Exception {
    /**
     * @var int HTTP код ответа, например 403, 404, 500 и т.д.
     */
    public $statusCode;

    /**
     * NotFoundHttpException constructor.
     * @param string $status
     * @param null $message
     * @param int $code
     * @param \Exception|null $previous
     */
    public function __construct($status, $message = null, $code = 0, \Exception $previous = null)
    {
        $this->statusCode = $status;
        parent::__construct($message, $code, $previous);
    }
}