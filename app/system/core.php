<?php

require_once __DIR__ . '/helpers/ArrayHelper.php';

/**
 * Формируем конфиг
 */
$config = \app\system\helpers\ArrayHelper::merge(
    require_once dirname(__DIR__) . '/config/main.php',
    require_once dirname(__DIR__) . '/config/main-local.php'
);

/**
 * Инициализируем автозагрузчик классов
 */
spl_autoload_register(function ($class) {
    // base directory for the namespace prefix
    $base_dir = dirname(dirname(__DIR__));

    // replace the namespace prefix with the base directory, replace namespace
    // separators with directory separators in the relative class name, append
    // with .php
    $file = $base_dir . '/' . str_replace('\\', '/', $class) . '.php';

    // if the file exists, require it
    if (file_exists($file)) {
        require_once $file;
    }
});

/**
 * Запускаем приложение
 */
(new App())->run($config);