# Инструкция по установке

1) Установить права доступа 777 на директорию web/files/blog

2) Выполнить SQL запрос:

        CREATE TABLE `blog` (
          `id` int(11) NOT NULL,
          `title` varchar(250) CHARACTER SET utf8 NOT NULL,
          `content` text CHARACTER SET utf8mb4 NOT NULL,
          `preview` varchar(50) DEFAULT NULL,
          `created_at` int(11) NOT NULL DEFAULT '0',
          `updated_at` int(11) NOT NULL
        ) ENGINE=InnoDB DEFAULT CHARSET=utf32;
        ALTER TABLE `blog`
          ADD PRIMARY KEY (`id`);
        ALTER TABLE `blog`
          MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;COMMIT;
        
3) Прописать данные от БД в файле app/config/main-local.php

4) Добавить конфиг для NGINX (Apache под рукой не было)

    server {
            listen 80;
            listen [::]:80;
    
    	charset 	 utf-8;
    	index        index.php;
        server_name	 <domain>;
    	root         /var/www/<domain>/web/;
    
            location / {
    		try_files $uri $uri/ =404;
            }
    
            location ~ \.php$ {
                    include snippets/fastcgi-php.conf;
                    fastcgi_pass unix:/run/php/php7.0-fpm.sock;
            }
    }