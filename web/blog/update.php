<?php

require_once dirname(dirname(__DIR__)) . '/app/system/core.php';

use app\models\Blog;
use app\system\exceptions\NotFoundHttpException;

#######################################################################################################################

/**
 * Получаем данные записи
 */
$saveData = [];
$id       = abs($_GET['id']);
$model    = Blog::findOne($_GET['id']);

/**
 * Если запись не найдена - выкидываем исключение
 */
if ($model === false){
    throw new NotFoundHttpException('Запись не найдена!');
}

/**
 * Если пришли POST данные - обрабатываем их и сохраняем.
 */
if (isset($_POST['submit'])){
    $saveData = Blog::save($model, $_POST);

    /**
     * Если данные успешно сохранены - делаем редирект на просмотр записи блога
     */
    if ($saveData['success'] === true){
        header('Location: /blog/view.php?id=' . $model['id']);
    }
}

/**
 * Выводим представление (вьюху)
 */
echo App::$get->view->render('blog/update', [
    'model'    => $model,
    'saveData' => $saveData,
]);