<?php

require_once dirname(dirname(__DIR__)) . '/app/system/core.php';

use app\models\Blog;
use app\system\exceptions\NotFoundHttpException;

#######################################################################################################################

/**
 * Получаем данные записи
 */
$id    = abs($_GET['id']);
$model = Blog::findOne($_GET['id']);

/**
 * Если запись не найдена - выкидываем исключение
 */
if ($model === false){
    throw new NotFoundHttpException('Запись не найдена!');
}

/**
 * Выводим представление (вьюху)
 */
echo App::$get->view->render('blog/view', [
    'model' => $model
]);