<?php

require_once dirname(__DIR__) . '/app/system/core.php';

use app\models\Blog;

#######################################################################################################################

echo App::$get->view->render('blog/index', [
    'all'   => Blog::findAll(),
    'count' => Blog::countAll(),
]);